from .event import Event3

class ShowEvent(Event3):
    NAME = "show"

    def perform(self):
        if not self.object.has_prop("wantToShow") and self.object2.has_prop("presenter"):
            self.fail()
            return self.inform("show.failed")
        self.inform("show")
