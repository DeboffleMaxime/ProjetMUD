
from .event import Event3
from .event import Event2

class HitEvent(Event2):
    NAME = "hit"

    def perform(self):
        if not self.object.has_prop("hitable"):
            self.fail()
            return self.inform("hit.failed")
        self.inform("hit")


class HitWithEvent(Event3):
    NAME = "hitWith"

    def perform(self):
        if not self.object.has_prop("hitablewith") or not self.object2.has_prop("weapon"):
            self.fail()
            return self.inform("hitWith.failed")
        self.inform("hitWith")
